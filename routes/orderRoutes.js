const express = require('express');
const router = express.Router();
const OrderController = require('../controllers/OrderController.js');
const auth = require('../auth.js');

// Order a product (OLD)
// router.post('/createOrder', auth.verify, (request, response) => {
// 	OrderController.orderProduct(request, response);
// })

// Order a product
router.post('/:productId', auth.verify, OrderController.createOrder);

// router.post("/", verify, verifyAdmin, ProductController.addProduct);

//  Get all products
// router.get("/all", ProductController.getAllProducts);
	


// Get authenticated user's order
router.post("/getOrder", auth.verify, (request, response) => {
	OrderController.getOrder(request.body).then((result) => {
		response.send(result);
	})
})


// Get all orders
router.get('/allOrder', auth.verify, auth.verifyAdmin, (request, response) => {
	OrderController.getAllOrders(request, response);
});

// Delete all orders
router.delete('/deleteOrders', auth.verify, auth.verifyAdmin, (request, response) => {
	OrderController.deleteAllOrders(request, response);
});

module.exports = router;	