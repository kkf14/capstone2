const express = require('express');
const router = express.Router();
const ProductController = require('../controllers/ProductController.js');
const auth = require('../auth.js');
const {verify, verifyAdmin} = auth;

// Create single product
router.post("/", verify, verifyAdmin, ProductController.addProduct);

// Get all products
router.get("/all", ProductController.getAllProducts);

// Get all active products
router.get('/allActive', (request, response) => {
	ProductController.getAllActiveProducts(request, response);
});

// Get single product
router.get('/:id', (request, response) => {
	ProductController.getProduct(request, response);
});

// Route to search for courses by course name
router.post('/search', ProductController.searchProductsByName);

// [SECTION] Route for retrieving a specific course
// Creating a route using the "/:parameterName" creates a dynamic route, meaning the url not static and may change depending on the information provided in the url.
router.get("/:courseId", ProductController.getProduct);

// Update product
// [SECTION] Route for updating a course (Admin)
// add JWT authentication for verifying if the user is an admin or not. Admin will only have access to update a course.
router.put("/:id", verify, verifyAdmin, ProductController.updateProduct);

// Archiving Product 
router.put("/:id/archive", verify, verifyAdmin, ProductController.archiveProduct);

// Activating Product
router.put("/:id/activate", verify, verifyAdmin, ProductController.activateProduct);

module.exports = router;