const express = require('express');
const router = express.Router();
const UserController = require('../controllers/UserController.js');
const auth = require('../auth.js');
const { verify, verifyAdmin } = auth;


// Register user
router.post("/register", (req, res) => {
	UserController.registerUser(req.body).then(result => res.send(result));
});

// Login user
router.post("/login", UserController.loginUser);

// Route for retrieving authenticated user details
router.get("/details", verify, UserController.getProfile)

// Get user details
// The auth.verify is a middleware function within the route.
	// router.post("/details", auth.verify, auth.verifyAdmin, (request, response) => {
	// 	UserController.getProfile(request.body).then((result) => {
	// 		response.send(result);
	// 	})
	// })

// User become admin
router.put('/authorize', auth.verify, auth.verifyAdmin, UserController.authorizeAdmin)

// Delete all users (This is for testing only)
router.delete('/delete', auth.verify, auth.verifyAdmin, (request, response) => {
	UserController.deleteAllUsers(request, response);
});

module.exports = router;