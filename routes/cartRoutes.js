const express = require('express');
const router = express.Router();
const CartController = require('../controllers/CartController.js');
const auth = require('../auth.js');

// Add to cart
router.post('/add', auth.verify, (request, response) => {
	CartController.addToCart(request, response);
})

// Get user cart
router.post("/get", auth.verify, (request, response) => {
	CartController.getCart(request.body).then((result) => {
		response.send(result);
	})
})

// Update Cart
router.put('/:cartId/:productId', auth.verify, (request, response) => {
	CartController.updateCartItemQuantity(request, response);
})

// Delete products in cart
router.delete('/:cartId/:productId', auth.verify, (request, response) => {
	CartController.deleteCartItem(request, response);
})

// Delete Cart (This is for testing only)
router.delete('/delete', auth.verify, auth.verifyAdmin, (request, response) => {
	CartController.deleteCartByUserId(request, response);
});

module.exports = router;