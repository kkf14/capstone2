//[SECTION] Modules and Dependencies
const mongoose = require('mongoose');

//[SECTION] Schema/Blueprint 
const user_schema = new mongoose.Schema({
  firstName: {
    type: String,
    required: [true, 'First Name is Required']
  },
  lastName: {
    type: String,
    required: [true, 'Last Name is Required']
  },
  email: {
    type: String,
    required: [true, "Email is required"],
  },
  password: {
    type: String,
    required: [true, "Password is required"],
  },
  isAdmin: {
    type: Boolean,
    default: false,
  },
  mobileNo: {
    type: String,
    required: [true, 'Mobile Number is Required']
  }
});

//[SECTION] Model 
module.exports = mongoose.model("User", user_schema);
