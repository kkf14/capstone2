const mongoose = require('mongoose');

// Schema
const cart_schema = new mongoose.Schema({
	userId : {
        type : String,
        required : [true, "User ID is required"]
    },
    products : [
    	{
    		productId : {
    			type : String,
       			required : [true, "Product ID is required"]
    		},
    		quantity : {
    			type : Number,
    			required : [true, "Quantity is required"]
    		},
    		subtotal : {
    			type : Number,
    			required : [true, "Subtotal is required"]
    		}
    	}
    ],
    totalAmount : {
        type : Number,
        required : [true, "Total Amount is required"]
    }
});

module.exports = mongoose.model("Cart", cart_schema);
