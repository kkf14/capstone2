const mongoose = require('mongoose');

// Schema
const order_schema = new mongoose.Schema({
	userId : {
        type : String,
        required : [true, "User ID is required"]
    },
    products : [
    	{
    		productId : {
    			type : String,
       			required : [true, "Product ID is required"]
    		},
    		quantity : {
    			type : Number,
    			required : [true, "Quantity is required"]
    		}
    	}
    ],
    totalAmount : {
        type : Number,
        required : [true, "Total Amount is required"]
    },
    purchasedOn : {
        type : Date,
        default : new Date()
    },
});

module.exports = mongoose.model("Order", order_schema);
