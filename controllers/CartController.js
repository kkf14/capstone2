const Cart = require('../models/Cart.js');
const Product = require('../models/Product.js');
const bcrypt = require('bcrypt');
const auth = require('../auth.js');

module.exports.addToCart = async (request, response) => {
  const { products } = request.body;

  // Only user can add to cart
  if (request.user.isAdmin) {
    return response.send('Action Forbidden, only users can order');
  }

  // Find the user's existing cart
  const existingCart = await Cart.findOne({ userId: request.user.id });

  // Checks if array is empty or if it's an array
  if (!Array.isArray(products) || products.length === 0) {
    return response.send({
      message: 'Products array is required and must not be empty.',
    });
  }

  // Initializing variables to be used in the loop and assigning of newOrder
	 const cartProducts = [];
	 let totalAmount = 0;

  // The contents of product array is being stored in productInfo as is replace with each iteration of the loop
	for (const productInfo of products) {
		// Deconstructs productInfo for easier coding
		const { productId, quantity } = productInfo;
		let subtotal = 0;

		// Fetch the product details of the matching ID from the database
		const product = await Product.findById(productId);

		// If there is an error product will be 'Null' as the fetch above is a promise and only returns the result or an error
		if (!product) {
			return response.send({
				message: `Product with ID ${productId} not found.`,
			});
		}

		totalAmount += product.price * quantity;
		subtotal = product.price * quantity;

		// .push is an array method which inserts a new array at the end
		cartProducts.push({
			productId,
			quantity,
			subtotal
		})
	}

  // If the user has an existing cart, update the products array
  if (existingCart) {
    existingCart.products.push(...cartProducts);
    existingCart.totalAmount += totalAmount;
    await existingCart.save();

    return response.send({
      message: 'Products added to existing cart!',
    });
  }
};

module.exports.getCart = async (request_body) => {
	return await Cart.findOne({ userId: request_body.userId}).then((cart, error) => {
		if(error) {
			return {
				message: "The ID was not found"
			}
		}

		return cart;
	}).catch(error => console.log(error));
}

module.exports.updateCartItemQuantity = async (request, response) => {
  const { cartId, productId } = request.params;
  const { quantity } = request.body;

  // Fetch the cart details from the database
  const cart = await Cart.findById(cartId);

  if (!cart) {
    return response.send({
      message: `Cart with ID ${cartId} not found.`,
    });
  }

  // Find the product in the cart
  const product = cart.products.find((product) => product._id.toString() === productId);

  if (!product) {
    return response.send({
      message: `Product with ID ${productId} not found in the cart.`,
    });
  }

  // Update the quantity of the product in the cart
  product.quantity = quantity;

  // Calculate the new totalAmount
  let totalAmount = 0;
  for (const product of cart.products) {
    const productDetail = await Product.findById(product.productId);
    console.log(productDetail);
    product.subtotal = productDetail.price * product.quantity;
    totalAmount += productDetail.price * product.quantity;
  }

  cart.totalAmount = totalAmount;

  // Save the updated cart
  await cart.save();

  return response.json({
    message: 'Quantity updated successfully.',
  });
};

module.exports.deleteCartItem = async (request, response) => {
  const { cartId, productId } = request.params;

  // Fetch the cart details from the database
  const cart = await Cart.findById(cartId);

  if (!cart) {
    return response.send({
      message: `Cart with ID ${cartId} not found.`,
    });
  }

  console.log(cart.products);
  // Find the index of the product in the cart
  const productIndex = cart.products.findIndex(
    (product) => product._id.toString() === productId
  );

  if (productIndex === -1) {
    return response.send({
      message: `Product with ID ${productId} not found in the cart.`,
    });
  }

  // Remove the product from the products array
  cart.products.splice(productIndex, 1);

  // Calculate the new totalAmount
  let totalAmount = 0;
  for (const product of cart.products) {
    const productDetail = await Product.findById(product.productId);
    totalAmount += productDetail.price * product.quantity;
  }

  cart.totalAmount = totalAmount;

  // Save the updated cart
  await cart.save();

  return response.send({
    message: 'Product deleted from cart successfully.',
  });
};

module.exports.deleteCartByUserId = async (request, response) => {
  const { userId } = request.body;

  const result = await Cart.deleteMany({ userId });

  return response.send({
    message: `${result.deletedCount} carts deleted for user with ID ${userId}.`,
  });
};