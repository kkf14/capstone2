const Product = require('../models/Product.js');
const bcrypt = require('bcrypt');
const auth = require('../auth.js');

module.exports.addProduct = (req, res) => {

	let newProduct = new Product({
		name : req.body.name,
		description : req.body.description,
		price : req.body.price
	});

	// Saves the created object to our database
	return newProduct.save().then((product, error) => {

		// Course creation successful
		if (error) {
			return res.send(false);

		// Course creation failed
		} else {
			return res.send(true);
		}
	})
	.catch(err => res.send(err))
};

module.exports.getAllProducts = (request, response) => {
	return Product.find({}).then(result => {
		console.log(result);
		return response.send(result);
	})
	.catch(err => res.send(err))
}



module.exports.getAllActiveProducts = (request, response) => {
	return Product.find({isActive: true}).then(result => {
		return response.send(result);
	})
}	


module.exports.getProduct = (req, res) => {
	return Product.findById(req.params.id).then(result => {
		console.log(result)
		return res.send(result)
	})
	.catch(err => res.send(err))
};


module.exports.getProduct = (request, response) => {
	return Product.findById(request.params.id).then(result => {
		return response.send(result);
	})
}

module.exports.searchProductsByName = async (req, res) => {
  try {
    const { productName } = req.body;

    // Use a regular expression to perform a case-insensitive search
    const products = await Product.find({
      name: { $regex: productName, $options: 'i' }
    });

    res.json(products);
  } catch (error) {
    console.error(error);
    res.status(500).json({ error: 'Internal Server Error' });
  }
};

module.exports.updateProduct = (request, response) => {
	let updatedProduct = {
		name: request.body.name,
		description: request.body.description,
		price: request.body.price
	};

	return Product.findByIdAndUpdate(request.params.id, updatedProduct).then ((product, error) => {
		if(error) {
			return response.send(false);
		} else {
			return response.send(true);
		}
	})
	.catch(err => response.send(err))
};

module.exports.archiveProduct = (request, response) => {
	let updateActiveField = {
		isActive: false
	}

	return Product.findByIdAndUpdate(request.params.id, updateActiveField)
	.then ((product, error) => {
		if(error){
			return response.send(false)

		// failed
		} else {
			return response.send(true)
		}
	})
	.catch(error => response.send(error))
};

module.exports.activateProduct = (request, response) => {
	let updateActiveField = {
		isActive: true
	}

	return Product.findByIdAndUpdate(request.params.id, updateActiveField).then ((product, error) => {
		if(error){
			return response.send(false)

		// failed
		} else {
			return response.send(true)
		}
	})
	.catch(error => response.send(error))
};
