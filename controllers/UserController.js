const User = require('../models/User.js');
const Cart = require('../models/Cart.js');
const bcrypt = require('bcrypt');
const auth = require('../auth.js');

module.exports.registerUser =  (reqBody) => {

	// Creates a variable newUser and instantiates a new User object using the mongoose model.
	// Uses the information from the request body to provide all the necessary information about the User object.
	let newUser = new User({
		firstName: reqBody.firstName,
		lastName: reqBody.lastName,
		email: reqBody.email,
		mobileNo: reqBody.mobileNo,
		// 10 - is the number of salt rounds that bcrypt algorithm will run in order to encrypt the password.
		password: bcrypt.hashSync(reqBody.password, 10)
	})

	// Create a cart for the registered user
  // const new_cart = new Cart({
  //   userId: savedUser._id, // Use the user's ID as the userId in the cart
  //   products: [],
  //   totalAmount: 0,
  // });

  // await new_cart.save();

	// Saves the created object to our database
	return newUser.save().then((user, error) => {
		// User registratioon failed
		if(error) {
			return false;
		// User registration successful
		} else {
			return true;
		}
		// catch() code block will handle other kinds of error.
		// prevent our app from crashing when an error occured in the backend server.
	}).catch(err => err)

};

// [Old registeredUser from capstone 2]
// module.exports.registeredUser = async (request_body) => {
//     const existingUser = await User.findOne({ email: request_body.email });
//     if (existingUser) {
//       return {
//         message: 'Email already exists. Cannot register a new user.',
//       };
//     }

//     const new_user = new User({
//       email: request_body.email,
//       password: bcrypt.hashSync(request_body.password, 10),
//     });

//     const savedUser = await new_user.save();

//     // Create a cart for the registered user
//     const new_cart = new Cart({
//       userId: savedUser._id, // Use the user's ID as the userId in the cart
//       products: [],
//       totalAmount: 0,
//     });

//     await new_cart.save();

//     return {
//       message: 'Successfully registered a user!',
//     };
// };


module.exports.loginUser = (request, response) => {
	return User.findOne({email: request.body.email}).then(result => {
		// User does not exist.
		console.log(result);
		if(result == null) {
			return response.send(false)
		} else {
			// Created the isPasswordCorrect variable to return the result of comparing the login form password and the database password.
			// compareSync() method is used to compare the non-encrypted password from the login form to the encrypted password from the database.
				// will return either "true" or "false"
			const isPasswordCorrect = bcrypt.compareSync(request.body.password, result.password);

			if(isPasswordCorrect) {
				// Generate an access token
				// Uses the "createAccessToken" method defined in the "auth.js" file. 
				// Returning an object back to the frontend application is a common practice to encure that information is properly labled and real world examples normally return more complex information represented by objects.
				return response.send({access: auth.createAccessToken(result)})
			} else {
				// If password is incorrect.
				return response.send(false);
			}
		}
	}).catch(err => response.send(err));

};

module.exports.getProfile = (req, res) => {
	return User.findById(req.user.id).then(result => {

		result.password = "";

		return res.send(result);

	})
	.catch(err => res.send(err))
};

module.exports.authorizeAdmin = (request_params) => {
	return User.findByIdAndUpdate(request_params.id, {isAdmin: true}).then ((user, error) => {
		if(error) {
			return response.send(false)
		}

		return response.send(true)
	})
}

module.exports.deleteAllUsers = async (request, response) => {
    const result = await User.deleteMany();

    response.json({
      message: 'All users have been deleted',
      deletedCount: result.deletedCount,
    });
};