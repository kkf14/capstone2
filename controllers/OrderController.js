const Order = require('../models/Order.js');
const Product = require('../models/Product.js');
const bcrypt = require('bcrypt');
const auth = require('../auth.js');

module.exports.orderProduct = async (request, response) => {
  	const {products} = request.body;

 //  	if(request.user.isAdmin){
	// 	return response.send('Action Forbidden, only users can order');
	// } 

  	// Checks if array is empty or if it's an array
	if (!Array.isArray(products) || products.length === 0) {
	    return response.send({
	    	message: 'Products array is required and must not be empty.',
	    });
	}

	// Initializing variables to be used in the loop and assigning of newOrder
    const orderProducts = [];
    let totalAmount = 0;

    // The contents of product array is being stored in productInfo as is replace with each iteration of the loop
	for (const productInfo of products) {
		// Deconstructs productInfo for easier coding
		const { productId, quantity } = productInfo;

		// Fetch the product details of the matching ID from the database
		const product = await Product.findById(productId);

		// If there is an error product will be 'Null' as the fetch above is a promise and only returns the result or an error
		if (!product) {
			return response.send({
			message: `Product with ID ${productId} not found.`,
			});
		}

		totalAmount += product.price * quantity;

		// .push is an array method which inserts a new array at the end
		orderProducts.push({
		productId,
		quantity,
		});

	}

    // Create the order
    const new_order = new Order({
      userId: request.user.id,
      products: orderProducts,
      totalAmount,
    });

    return new_order.save().then((created_order, error) => {
		if(error) {
			return response.send({
				message: error.message
			})
		}

		return response.send ({
			message: 'Order created succesfully!'
		});
	}).catch(error => console.log(error));
};



module.exports.createOrder = async (req, res) => {
  try {
    const { productId } = req.params;
    const { quantity } = req.body;

    // Validate input
    if (!quantity || typeof quantity !== 'number') {
      return res.send(false);
    }

    // Fetch product details
    const product = await Product.findById(productId);

    if (!product) {
      return res.send(false);
    }

    // Calculate total amount
    const totalAmount = product.price * quantity;

    // Create order
    const orderData = {
      userId: req.user.id, // Replace with actual user ID
      products: [{ productId, quantity }],
      totalAmount,
    };

    const order = new Order(orderData);
    await order.save();

    res.send(true);
  } catch (error) {
    console.error(error);
    res.send(false);
  }
};


module.exports.getOrder = async (request_body) => {
	return await Order.findOne({ userId: request_body.userId}).then((order, error) => {
		if(error) {
			return {
				message: "The ID was not found"
			}
		}

		return order;
	}).catch(error => console.log(error));
}

module.exports.getAllOrders = (request, response) => {
	return Order.find({}).then(result => {
			return response.send(result);
	})
}


module.exports.deleteAllOrders = async (request, response) => {
  try {
    // Delete all orders
    const result = await Order.deleteMany();

    response.json({
      message: 'All orders have been deleted',
      deletedCount: result.deletedCount,
    });
  } catch (error) {
    console.error(error);
    response.status(500).json({ message: 'Internal server error' });
  }
};