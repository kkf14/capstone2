// Server Variables
const express = require('express');
const mongoose = require('mongoose');
const cors = require('cors');
const userRoutes = require('./routes/userRoutes.js');
const productRoutes = require('./routes/productRoutes.js');
const orderRoutes = require('./routes/orderRoutes.js');
const cartRoutes = require('./routes/cartRoutes.js');

// Server Setup
const app = express();

// Environment Setup
const port = 4000;

// Middleware
app.use(express.json());
app.use(express.urlencoded({extended: true}));
app.use(cors()); 

// Routes
app.use('/users', userRoutes);
app.use('/products', productRoutes);
app.use('/orders', orderRoutes);
app.use('/cart', cartRoutes);

// Database Connection
// ALWAYS CHECK username and password and database directory
mongoose.connect(`mongodb+srv://admin:admin1234@303-fulcher.0hsltfc.mongodb.net/b303-capstone2-db?retryWrites=true&w=majority`, {
	useNewUrlParser: true,
	useUnifiedTopology: true
});

mongoose.connection.on('error', () => console.log("Can't connect to database"))
mongoose.connection.once('open', () => console.log("Connected to MongoDB!"))

app.listen(process.env.PORT || port, () => {
	console.log(`E-Commerce API is now running at localhost:${process.env.PORT || port}`);
})

module.exports = app;